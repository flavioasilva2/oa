#include <stdio.h>

#include "namereader.h"
#include "hash.h"

#define HASH_TABLE_SIZE 3000

void collisionsFrom0to(int n);

int main(int argc, char **argv){
    if(argc != 2){
        printf("favor usar:\n%s <arquivo com os nomes>\n", argv[0]);
        return 1;
    }
    char **names = getNames(argv[1]);
    hashTable *table = initHashTable(HASH_TABLE_SIZE);
    for(int i = 0; names[i] != NULL; i++){
        int addr = simpleHash(names[i]) % HASH_TABLE_SIZE;
        table[addr].colisions++;
        insertKey(table, HASH_TABLE_SIZE, names[i], addr);
    }
    printf("endereço|           chave|encadeamento|colisões\n");
    for(int i = 0; i < HASH_TABLE_SIZE; i++){
        printf("%8d|%16s|%12d|%8d\n", i, table[i].key, table[i].next, (table[i].colisions == -1 || table[i].colisions == 0) ? 0 : table[i].colisions);
    }
    int colisions = 0;
    for(int i = 0; i < HASH_TABLE_SIZE; i++){
        if(table[i].colisions > 0) colisions += table[i].colisions;
    }
    printf("colisões: %d\n", colisions);
    collisionsFrom0to(10);
    return 0;
}

/**
 * Calcula a probabilidade de n colisões em um determinado registro. 
*/
void collisionsFrom0to(int n){
    double tmp = 0;
    printf("\n\nPoisson\n");
    for(int i = 0; i < n; i++){
        if(i == 0){
            printf("Endereços com %d registros: %f\n", i, poisson(HASH_TABLE_SIZE, 1000, i)*HASH_TABLE_SIZE);
        }
        if(i == 1){
            printf("Endereços com %d registros: %f\n", i, poisson(HASH_TABLE_SIZE, 1000, i)*HASH_TABLE_SIZE);
        }
        if(i > 1){
            tmp += poisson(HASH_TABLE_SIZE, 1000, i);
            printf("Endereços com %d registros: %f\n", i, tmp * HASH_TABLE_SIZE);
        }
    }
}