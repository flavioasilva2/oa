#include <stdio.h>
#ifndef _NAMEREADER_H_
#define _NAMEREADER_H_

typedef struct namesLinkedList {
    char *name;
    struct namesLinkedList *next;
}namesLinkedList;

char **getNames(char*);

char *__getLine(char *, int, int);

unsigned long __fileSize(FILE*);

char *__readFile(FILE*, unsigned long);

#endif // _NAMEREADER_H_