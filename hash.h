#ifndef _HASH_H_
#define _HASH_H_

#define MAX_HASH_KEY_SIZE 16
#define PRIME 25849;

typedef struct hashTableEntry{
    char *key; // chave passada para a função de hash.
    int next; // indice na tabela de hash da próxima entrada, usando encadeamento progressivo.
    /**
     * contador de colisões para esse endereço.
     * -1 quando quando o endereço não está sendo usado pelo endereço natural
     * 0 quando não houveram colisões e o endereço está sendo usado por h(endereço natural)
     * 1++ dá o número de colisões 
    */
    int colisions;
}hashTableEntry;

typedef struct hashTableEntry hashTable;

int simpleHash(char*);

double poisson(int, int, int);

long long int factorial(int);

hashTableEntry *initHashTable(int);

void insertKey(hashTable*, int, char*, int);

int __getNextFreeAddress(hashTable*, int, int);

#endif // _HASH_H_