#include <stdio.h>
#include <stdlib.h>

#include "namereader.h"

#ifndef _NAMEREADER_
#define _NAMEREADER_

/**
 * Lê arquivo com nomes
 * 
 * @param fileName Nome do arquivo que contém a lista de nomes, um por linha
 * @return Lista de strings com os nomes lidos
*/
char **getNames(char *fileName){
    FILE *f = fopen(fileName, "rb");
    if(f == NULL) return NULL;
    unsigned long fSize = __fileSize(f);
    char *fileContent = __readFile(f, fSize);
    fclose(f);
    f=NULL;

    namesLinkedList *linkedListTop = NULL, *aux;
    int start = 0, linkedListSize = 0;
    for(int i = 0; i < fSize; i++){
        if(fileContent[i] == '\n'){
            if(linkedListTop == NULL){
                aux = linkedListTop = (namesLinkedList *)malloc(sizeof(namesLinkedList));
            }
            else{
                aux->next = (namesLinkedList *)malloc(sizeof(namesLinkedList));
                aux = aux->next;
            }
            aux->name = __getLine(fileContent, start, i);
            aux->next = NULL;
            start = i+1;
            linkedListSize++;
        }
    }
    free(fileContent);
    fileContent = NULL;

    aux = linkedListTop;
    int namesListSize = linkedListSize + 1, i = 0;
    char **namesList = (char **)malloc(sizeof(char *) * namesListSize);
    namesList[namesListSize - 1] = NULL;
    while(aux != NULL){
        namesList[i] = aux->name;
        i++;
        linkedListTop = aux;
        aux = aux->next;
        free(linkedListTop);
    }
    return namesList;
}

/**
 * Função auxiliar
*/
char *__getLine(char *file, int start, int end){
    int lineSize = end - start + 1;
    char *line = (char *)malloc(sizeof(char) * lineSize);
    for(int i = start,j = 0; i <= end; i++, j++)
        line[j] = file[i];
    line[lineSize - 1] = '\0';
    return line;
}

/**
 * Função auxiliar
*/
unsigned long __fileSize(FILE *file){
    if(file == NULL) return (unsigned long)0;
    unsigned long size = 0;
    long currentPos = ftell(file);
    fseek(file, 0, SEEK_SET);
    while(fgetc(file) != EOF)
        size++;
    fseek(file, currentPos, SEEK_SET);
    return size;
}

/**
 * Função auxiliar
*/
char *__readFile(FILE *file, unsigned long size){
    char *out = (char *)malloc(sizeof(char) * size);
    long currentPos = ftell(file);
    fseek(file, 0, SEEK_SET);
    for(int i = 0; i < size; i++){
        out[i] = fgetc(file);
    }
    fseek(file, currentPos, SEEK_SET);
    return out;
}

#endif // _NAMEREADER_