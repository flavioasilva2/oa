#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "hash.h"

#ifndef _HASH_
#define _HASH_

/**
 * Função de hash simples, baseada no exemplo dos slides de aula
 * 
 * @param key Chave primária do registro.
 * @return Endereço na tabela hash
*/
int simpleHash(char *key){
    char s[MAX_HASH_KEY_SIZE];
    int keyLength = strlen(key);
    unsigned int sum = 0;

    for(int i = 0; i < MAX_HASH_KEY_SIZE; i++){
        if(i >= keyLength)
            s[i] = ' ';
        else
            s[i] = key[i];
    }

    for(int i = 0; i < MAX_HASH_KEY_SIZE; i+=2)
        sum += (unsigned int)(((s[i]) << 8) | s[i+1]);
    return sum % PRIME;
}

/**
 * Distribuição de Poisson
 * 
 * @param n Número de endereços disponíveis
 * @param r Número de chaves a serem armazenadas
 * @param k Número de registros associados a um endereço determinado.
 * @return a probabilidade de um endereço ter k registros para ele
*/
double poisson(int n, int r, int k){
    return (pow(((double)r/(double)n), k) * pow(M_E, -((double)r/(double)n))) / factorial(k);
}

long long int factorial(int i){
    if(i == 1 || i == 0) return i;
    return i * factorial(i - 1);
}

/**
 * Função para inicializar uma tabela de hash com n entradas
 * 
 * @param size Número de entradas na tabela
 * @return Ponteiro para a nova tabela
*/
hashTable *initHashTable(int size){
    hashTable *table = (hashTable *)malloc(sizeof(hashTableEntry) * size);
    for(int i = 0; i < size; i++){
        (table[i]).key = NULL;
        (table[i]).next = -1;
        (table[i]).colisions = -1;
    }
    return table;
}

/**
 * Insere uma chave no endereço desejado, usando encadeamento progressivo
*/
void insertKey(hashTable *t, int tableSize, char *key, int address){
    if(t[address].key == NULL){
        t[address].key = key;
        return;
    }
    t[__getNextFreeAddress(t, tableSize, address)].key = key;
}

int __getNextFreeAddress(hashTable *t, int tableSize, int baseAddr){
    if(t[baseAddr].next == -1){
        int i = 1;
        while((t[baseAddr + i].key != NULL) && (baseAddr + i < tableSize)) i++;
        if(baseAddr + i == tableSize){
            i = -baseAddr;
            while((t[baseAddr + i].key != NULL) && (i < 0)) i++;
        }
        t[baseAddr].next = baseAddr + i;
        return baseAddr + i;
    }
    return __getNextFreeAddress(t, tableSize, t[baseAddr].next);
}

#endif // _HASH_
