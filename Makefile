build: buildDir namereader hash
	gcc -o build/oa -L. build/namereader.o build/hash.o oa.c -lm

clean:
	@rm -Rf build/

namereader: buildDir
	gcc -o build/namereader.o -c namereader.c

hash: buildDir
	gcc -o build/hash.o -c hash.c

buildDir:
	@mkdir -p build
